[systemd unit](https://www.freedesktop.org/software/systemd/man/systemd.unit.html)
for updating digital ocean DNS with the host's external IP, using 
[tunix/digitalocean-dyndns](https://github.com/tunix/digitalocean-dyndns)

I use it to keep my homelab connected to the internet.

## Use

1. `ln -s /opt/dodyndns/dodyndns@.service /etc/systemd/system`
1. copy `example.com.env` into `/opt/dodyndns` and fill in the variables
1. enable and start the service

```
$ git clone git@gitlab.com:accidentalcc/dodyndns-systemd.git /opt/dodyndns
$ ln -s /opt/dodyndns /etc/systemd/system
$ cd /opt/dodyndns
$ cp example.com.env homelab.lol.env
$ vim homelab.lol.env
$ systemctl daemon-reload
$ systemctl enable --now dodyndns@homelab.lol
```
